##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
ARG release=zed

FROM registry.yaook.cloud/yaook/nova-compute-${release}-ubuntu:4.1.141

# Because ARGs defined before a FROM are not available after the FROM,
# ARGs that should be available after the FROM must be re-defined.
ARG release

USER root

COPY files/pipeline.yaml /etc/ceilometer/pipeline.yaml

RUN set -eux ; \
    export DEV_PACKAGES="python3-dev git gcc"; \
    apt update; \
    apt install -y --no-install-recommends $DEV_PACKAGES; \
    (git clone https://github.com/openstack/requirements.git --depth 1 --branch stable/${release} \
    || git clone https://github.com/openstack/requirements.git --depth 1 --branch ${release}-eol \
    || git clone https://github.com/openstack/requirements.git --depth 1 --branch unmaintained/${release}); \
    sed -i '/^ceilometer===.*/d' /requirements/upper-constraints.txt; \
    # We need to updaddte importlib-metadata to work sith python3.8
    if [ "$release" = "train" ]; then \
      sed -i 's/^importlib-metadata===.*/importlib-metadata===1.6.0/' /requirements/upper-constraints.txt; \
    fi; \
    (git clone https://github.com/openstack/ceilometer.git --depth 1 --branch stable/${release} \
    || git clone https://github.com/openstack/ceilometer.git --depth 1 --branch ${release}-eol \
    || git clone https://github.com/openstack/ceilometer.git --depth 1 --branch unmaintained/${release}); \
    pip3 install wheel; \
    pip3 install -c requirements/upper-constraints.txt PyMySQL python-memcached pymemcache gnocchiclient ceilometer/; \
    apt-get purge --auto-remove -y $DEV_PACKAGES; \
    rm -rf /var/lib/apt/lists/*; \
    rm -rf requirements ceilometer

USER nova

